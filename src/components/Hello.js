import React from 'react';
import {Text, View} from 'react-native';

const Hello = props => {
  return (
    <View>
      <Text>
        Heyo, {props.name} yang berumur {props.umur}
      </Text>
    </View>
  );
};

Hello.defaultProps = {
  name: 'Budi',
};

export default Hello;
