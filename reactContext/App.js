// 1. import Context
import React from 'react';
import {Text, View} from 'react-native';
import {ActionsScreen, DisplayScreen} from './src/screens';
import {CounterProvider} from './src/context/counter';

// 2. inisialisasi
// const MyContext = createContext({number: 0});

//contoh child component
//gunakan consumer

// 3. membungkus child component
const App = () => {
  return (
    <View>
      <Text>React Context</Text>
      <CounterProvider>
        <ActionsScreen />
        <DisplayScreen />
      </CounterProvider>
    </View>
  );
};
export default App;
