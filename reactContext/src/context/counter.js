import React, {createContext, PureComponent} from 'react';

const CounterContext = createContext({number: 0});

class CounterProvider extends PureComponent {
  state = {
    counter: 0,
  };

  componentDidMount() {
    this.setState({counter: 50});
  }

  updateCounter() {
    this.setState({counter: 100});
  }

  render() {
    const {children} = this.props;
    const data = {
      counter: this.state.counter,
      updateData: () => this.updateCounter(),
    };
    return (
      <CounterContext.Provider value={data}>{children}</CounterContext.Provider>
    );
  }
}

const CounterConsumer = CounterContext.Consumer;

export {CounterConsumer, CounterProvider};
export default CounterContext;
