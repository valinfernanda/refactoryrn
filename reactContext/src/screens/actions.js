import React from 'react';
import {View, Text, Button} from 'react-native';
import {CounterConsumer} from '../context/counter';

const Actions = () => {
  return (
    <View style={{padding: 5, borderColor: 'red', borderWidth: 3}}>
      <Text>Component Actions</Text>
      <CounterConsumer>
        {value => {
          return (
            <View>
              <Text>data counter: {value.counter}</Text>
              <Button
                onPress={value.updateData}
                title="Update Context Number"
                color="purple"
                accessibilityLabel="Learn more"
              />
            </View>
          );
        }}
      </CounterConsumer>
    </View>
  );
};

export default Actions;
