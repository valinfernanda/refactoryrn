import React from 'react';
import {View, Text} from 'react-native';
import {CounterConsumer} from '../context/counter';

const Display = () => {
  return (
    <View style={{padding: 5, borderColor: 'blue', borderWidth: 3}}>
      <Text>Component Display</Text>
      <CounterConsumer>
        {value => {
          return (
            <View>
              <Text>data anda : {value.counter}</Text>
            </View>
          );
        }}
      </CounterConsumer>
    </View>
  );
};

export default Display;
