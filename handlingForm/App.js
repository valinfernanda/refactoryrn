import React, {Component} from 'react';
import {Text, View, TextInput, Button, Alert} from 'react-native';

class App extends Component {
  state = {
    value: '',
    isShow: false,
  };

  onChangeText(text) {
    this.setState({value: text, isShow: false});
  }

  onPress() {
    this.setState({isShow: true});
  }
  render() {
    const {value, isShow} = this.state;
    return (
      <View>
        <Text>Handling input</Text>
        <TextInput
          onChangeText={text => this.onChangeText(text)}
          value={value}
          style={{borderWidth: 1, height: 40, borderColor: 'grey'}}
        />
        <Text>Output: {value}</Text>
        <Button
          onPress={() => this.onPress()}
          title="Klik disini"
          color="blue"
        />
        {isShow && <Text>{value}</Text>}
      </View>
    );
  }
}

export default App;
