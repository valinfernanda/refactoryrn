import React from 'react';
import {Text, View} from 'react-native';

import Hello from './src/components/Hello';
import Article from './src/components/Article';

const App = () => {
  return (
    <>
      <View>
        <Text>Hello Guys</Text>
        <Hello umur="24" />
        <Hello name="Valin" umur="24" />
        <View>
          <Article title="Weys">
            <Text>Ini children dari article</Text>
            {/* komponen text diatas diapit oleh komponen article, akan dianggap sbg
            komponen children */}
          </Article>
        </View>
      </View>
    </>
  );
};
export default App;
