import React from 'react';
import {View, Text} from 'react-native';
import Hello from './Hello';

const Article = props => {
  return (
    <>
      <View>
        <Hello />
        <Text>{props.title}</Text>
        <Text>{props.children}</Text>
      </View>
    </>
  );
};

export default Article;
